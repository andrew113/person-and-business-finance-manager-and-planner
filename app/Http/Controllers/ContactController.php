<?php
namespace App\Http\Controllers;

use App\Http\Controllers\OAD\OADController;
use Illuminate\Http\Request;
use App\Traits\TableHelpers;

class ContactController extends OADController
{
    use TableHelpers;

    protected $model = 'App\Models\Contact';

    public function index(Request $request)
    {

        \User::checkAccess('contacts',['view','full']);
        
        //running builder from the trait
        $response = $this->listBuilder(
            new $this->model, //class
            config('project.table.editDelete'),
            function($q) { //function exnending query
                return $q->list();
            },
            null, //filters array of callbacks
            function($q,$searchTerm) { //custom search query
                $searchTerm = addslashes($searchTerm);

                return $q->orWhereHas('addresses',function($q) use ($searchTerm) {
                    $q->whereRaw(" CONCAT_WS(', ',unit,address,city,province,postal)  LIKE '%{$searchTerm}%' ");
                })
                ->orWhereHas('phone_numbers',function($q) use ($searchTerm) {
                    $q->where('number','LIKE','%'.$searchTerm.'%');
                })
                ->orWhereHas('emails',function($q) use ($searchTerm) {
                    $q->where('email','LIKE','%'.$searchTerm.'%');
                });

            }
        );

        return response()->json( $response );
    }


    public function show(Request $request)
    {

        \User::checkAccess('contacts',['view','full']);

        $model          = $this->model::findOrMkNew($request->hash);
        $mainFormValues = $model->buildFields()->getFieldModelValues();

        return response()->json(
            [
                'status'    => 'success',
                'hash'      => $request->hash,
                'forms'    => [
                    'main'  => [
                        'fields'    => $model->form_fields['main'],
                        'values'    => $mainFormValues
                    ]   
                ]
            ],
            200
        );
    }

    public function store(Request $request) {

        \User::checkAccess('contacts','full');

        $model = $this->model::findOrMkNew($request->hash);

        $model->validateForm($request->forms['main']['values'])
                ->saveContact( $request->hash , $request->forms['main']['values'] );

    }

    public function getContactHistory(Request $request) {

        $show_history = false;
        $addresses = [];
        $emails = [];
        $numbers = [];

        if ($model = $this->model::find($request->hash)) {
            
            if ($res = $model->addresses()->with('json_assignable')->get()) {
                foreach ($res as $item) {
                    foreach ($item->json_assignable as $history_record) {
                        $data = $history_record->table_data;
                        $data['hash']       = $history_record->hash;
                        $data['created_at'] = $history_record->created_at;
                        $addresses[] = $data;
                    }
                }
                if (!$show_history) $show_history = count($addresses) > 0;
            }

            if ($res = $model->phone_numbers()->with('json_assignable')->get()) {
                foreach ($res as $item) {
                    foreach ($item->json_assignable as $history_record) {
                        $data = $history_record->table_data;
                        $data['hash']       = $history_record->hash;
                        $data['created_at'] = $history_record->created_at;
                        $numbers[] = $data;
                    }
                }
                if (!$show_history) $show_history = count($numbers) > 0;
            }

            if ($res = $model->emails()->with('json_assignable')->get()) {
                foreach ($res as $item) {
                    foreach ($item->json_assignable as $history_record) {
                        $data = $history_record->table_data;
                        $data['hash']       = $history_record->hash;
                        $data['created_at'] = $history_record->created_at;
                        $emails[] = $data;
                    }
                }
                if (!$show_history) $show_history = count($emails) > 0;
            }
            
            return response()->json([
                'show_history'  => $show_history,
                'numbers'       => $numbers,
                'addresses'     => $addresses,
                'emails'        => $emails
            ]);
        }

        return '';
        
    }

}
