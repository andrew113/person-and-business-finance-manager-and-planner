<?php

namespace App\Http\Controllers;

use App\Http\Controllers\OAD\OADController;
use Illuminate\Http\Request;
use App\Traits\TableHelpers;

class AccountController extends OADController
{
    use TableHelpers;

    protected $model = 'App\Models\Account';

    public function index(Request $request)
    {

        //running builder from the trait
        $response = $this->listBuilder(
            new $this->model, //class
            config('project.table.editDelete')
            //function exnending query
            //filters array of callbacks
            //custom search query
        );

        return response()->json( $response );
    }


    public function show(Request $request)
    {
        $model          = $this->model::find($request->hash) ?? new $this->model;
        $modelsNvalues  = $model->buildFields()->getFieldModelValues();

        return response()->json(
            [
                'status'    => 'success',
                'hash'      => $request->hash,
                'forms'    => [
                    'main'  => [
                        'fields'    => $model->form_fields['main'],
                        'values'    => $modelsNvalues
                    ]
                    
                ]
            ],
            200
        );
    }

    public function store(Request $request) {

        $model = $this->model::find($request->hash) ?? new $this->model;

        $model->validateForm($request->forms['main']['values'])
              ->store([ 'hash' => $request->hash ], $request->forms['main']['values']);

    }

    public function list(Request $request) {

        $model = $this->model::select(\DB::raw("hash, CONCAT_WS(' ',name,number) as name "));

        //lookup matching pair for value
        if ($request->hash) {
            return $model->where('hash',$request->hash)->get()->pluck('name','hash');
        }

        //perform search for results
        if ($request->search) {
            $model->where('name','LIKE','%' . $request->search . '%')
                    ->orWhere('number','LIKE','%' . $request->search . '%');
        }
        if ($request->clients_id) {
            $model->where('clients_id',$request->clients_id);
        }
        return $model->limit(10)->orderBy('name')->get()->pluck('name','hash');
    }

}
