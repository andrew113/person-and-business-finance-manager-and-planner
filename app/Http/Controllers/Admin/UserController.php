<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\OAD\OADController;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Traits\TableHelpers;

class UserController extends OADController
{
    use TableHelpers;

    protected $model = 'App\Models\User';

    public function index(Request $request)
    {
        //running builder from the trait
        $response = $this->listBuilder(
            new $this->model(), //class
            [ [ 'action' => 'edit', 'text'   => 'Edit'] ],
            function ($q) {
                return $q->list()->joinTable('users_roles','roles_id','id');
            }
        );

        return response()->json( $response );
    }

    public function show(Request $request)
    {
  
        $model = $request->hash ? $this->model::where('hash',$request->hash)->first() : new $this->model();

        $modelsNvalues = $model->buildFields()->getFieldModelValues('main','id');

        return response()->json(
            [
                'status'    => 'success',
                'hash'      => $request->hash,
                'forms'    => [
                    'main'  => [
                        'fields'    => $model->form_fields['main'],
                        'values'    => $modelsNvalues
                    ]
                    
                ]
            ], 
            200
        );
    }

    public function store(Request $request) {
        
        $model = $request->hash ? $this->model::where('hash',$request->hash)->first() : new $this->model();
        
        $valid_roles = [
                        'email' => [
                                'required',
                                'email',
                                Rule::unique('users')->ignore($model),
                            ],
                        'password' => config('project.password_rules')
                    ];
        if ($request->hash) { 
            // for existing users - can keep field blank
            $valid_roles['password'] = array_merge(config('project.password_rules'),['sometimes','nullable']);
        }
        
        $model->validateForm($request->forms['main']['values'],true,$valid_roles)
              ->store([ 'hash' => $request->hash ], $request->forms['main']['values']);

    }

    public function destroy($hash) {
        return false;
    }

    public function list(Request $request) {
        $mPath = $this->model;
        $model = $mPath::list()->active()->select(['hash','name']);

        //lookup matching pair for value
        if ($request->hash) {
            return $model->where('hash',$request->hash)->get()->pluck('name','hash');
        }
        
        //perform search for results
        if ($request->search) {
            $model->where('name','LIKE','%' . $request->search . '%');
        }
        
        return $model->limit(10)->orderBy('name')->get()->pluck('name','hash');
    }

}
