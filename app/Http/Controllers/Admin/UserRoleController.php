<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\OAD\OADController;
use Illuminate\Http\Request;
use App\Http\Controllers\OAD\LayoutController;
use App\Models\UserRolePermission;

class UserRoleController extends OADController
{

	protected $model = 'App\Models\UserRole';

	public function index(Request $request)
    {

        //running builder from the trait
        $response = $this->listBuilder(
            new $this->model(), //class
            config('project.table.editDelete'),
            function($q) {
                return $q->list();
            }
        );

        return response()->json( $response );
    }

	public function show(Request $request)
    {

        $model          = $this->model::findOrMkNew($request->hash);
        $modelsNvalues  = $model->buildFields()->getFieldModelValues('main','id');

		$menu = new LayoutController();

        return response()->json(
            [
                'status'    => 'success',
                'hash'      => $request->hash,
                'forms'    => [
                    'main'  => [
                        'fields'    => $model->form_fields['main'],
                        'values'    => $modelsNvalues                    
                    ],
                    'tree'  => [
                        'fields' => $menu->sections_tree(),
                        'values' => UserRolePermission::select('id','permission','sections_id')->where('users_roles_id',$request->hash)->get()
                    ]
                ],
            ],
            200
        );
    }

	public function store(Request $request) {

		$model = $this->model::findOrMkNew($request->hash);

        $model->validateForm($request->forms['main']['values'])
                ->savePermissions([ 'id' => $request->hash ], $request->forms );
    }
    
    public function destroy($hash) {
        
        $role = $this->model::withCount('users')->find($hash);

        if ($role->users_count) {
            return response()->json(['status' => 'error', 'res' => 'This permission is used by ' . $role->users_count . ' users']);
        }

        if ($this->model::destroy($hash)) {
            return response()->json(['status' => 'success', 'res' => 'Record deleted']);
        }

        return response()->json(['status' => 'error', 'res' => 'Failed to delete']);
    }


}
