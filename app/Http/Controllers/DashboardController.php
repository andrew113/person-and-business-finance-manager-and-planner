<?php
namespace App\Http\Controllers;

use Carbon;
use App\Http\Controllers\OAD\OADController;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\TransCategory;

class DashboardController extends OADController
{

    public function expenses_stat(Request $request) {

        $data = [ 'fixed' => [], 'variable' => [] ];

        
        for ($i = 5; $i >= 0; $i-- ) {
            $month                  = Carbon::now()->subMonths($i)->month;
            $year                   = Carbon::now()->subMonths($i)->year;
            $data['fixed'][]        = Transaction::otype($request->otype)->isExpense('fixed')->approved()->monthYear($month,$year)->get()->sum(function($row) {
                return $row['debit'] - $row['credit'];
            });
            $data['variable'][]     = Transaction::otype($request->otype)->isExpense('variable')->approved()->monthYear($month,$year)->get()->sum(function($row) {
                return $row['debit'] - $row['credit'];
            });
        }

        return response()->json([
            'labels'    => $this->last_6_momths_labels(),
            'datasets'  => [
                [ 'label'=> 'Fixed', 'data' => $data['fixed'], 'backgroundColor' => '#ffff0085', 'borderColor' => '#ffff0085', 'borderWidth' => 0 ],
                [ 'label' => 'Variable', 'data' => $data['variable'], 'backgroundColor' => '#4dc0b586', 'borderColor' => '#4dc0b5', 'borderWidth' => 0 ]
            ]
        ]);

    }

    public function income_vs_exp(Request $request) {

        $data = [ 'income' => [], 'expense' => [] ];
        
        for ($i = 5; $i >= 0; $i-- ) {
            $month              = Carbon::now()->subMonths($i)->month;
            $year               = Carbon::now()->subMonths($i)->year;
            $data['income'][]   = Transaction::otype($request->otype)->monthYear($month,$year)->isIncome()->approved()->get()->sum(function($row) {
                return $row['credit'] - $row['debit'];
            });
            $data['expense'][]  = Transaction::otype($request->otype)->monthYear($month,$year)->isExpense()->approved()->get()->sum(function($row) {
                return $row['debit'] - $row['credit'];
            });
        }

        return response()->json([
            'labels'    => $this->last_6_momths_labels(),
            'datasets'  => [
                [ 'label'=> 'Income', 'data' => $data['income'], 'backgroundColor' => 'green', 'borderColor' => 'green', 'borderWidth' => 0 ],
                [ 'label' => 'Expense', 'data' => $data['expense'], 'backgroundColor' => 'blue', 'borderColor' => 'blue', 'borderWidth' => 0 ]
            ]
        ]);

    }

    public function expense_by_type(Request $request) {

        $data       = [];
        $datasets   = [];
        $expenses   = TransCategory::expenses()->get();

        foreach($expenses as $expense) $data[$expense->hash] = [];
        
        for ($i = 5; $i >= 0; $i-- ) {

            $month              = Carbon::now()->subMonths($i)->month;
            $year               = Carbon::now()->subMonths($i)->year;

            foreach($expenses as $expense) {
                $data[$expense->hash][]   = Transaction::otype($request->otype)->monthYear($month,$year)->approved()->where('transactions_categories_id',$expense->hash)->get()->sum(function($row) {
                    return $row['debit'] - $row['credit'];
                });

            }
            
        }

        foreach($expenses as $expense) {
            $dataset = [ 'label'=> $expense->name, 'data' => $data[$expense->hash] ];
            if ($expense->chart_color) {
                $dataset['backgroundColor'] = $expense->chart_color . '40';
                $dataset['borderColor'] = $expense->chart_color;
            }
            $datasets[] = $dataset;
        }

        return response()->json([
            'labels'    => $this->last_6_momths_labels(),
            'datasets'  => $datasets
        ]);

    }

    public function last_6_momths_labels() {
        $return = [];
        for ($i = 5; $i >= 0; $i-- ) {
            $return[] = Carbon::now()->subMonths($i)->monthName;
        }

        return $return;
    }

   

}
