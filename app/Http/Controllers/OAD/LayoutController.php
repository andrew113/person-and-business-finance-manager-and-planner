<?php

namespace App\Http\Controllers\OAD;

use OADSOFT\SPA\Http\Controllers\LayoutController as OADLayoutController;
use App\Models\OAD\Section;

class LayoutController extends OADLayoutController
{
    // you can override OADLayoutController or add methods here
}
