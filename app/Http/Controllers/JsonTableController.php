<?php
namespace App\Http\Controllers;

use App\Http\Controllers\OAD\OADController;
use Illuminate\Http\Request;
use App\Traits\TableHelpers;

class JsonTableController extends OADController
{
    use TableHelpers;

    protected $model = 'App\Models\JsonTable';

    public function delete(Request $request) {

        if ($this->model::destroy($request->hash)) {
            return response()->json(['status' => 'success', 'res' => 'Record deleted']);
        }

        return response()->json(['status' => 'error', 'res' => 'Failed to delete']);
    }

}
