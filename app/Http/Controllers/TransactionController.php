<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Transaction;
use App\Traits\TableHelpers;
use Illuminate\Http\Request;
use App\Models\TransCategory;
use App\Models\TransImportRule;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\OAD\OADController;
use Auth;

class TransactionController extends OADController
{
    use TableHelpers;

    protected $model = 'App\Models\Transaction';

    public function index(Request $request)
    {

        //running builder from the trait
        $response = $this->listBuilder(
            new $this->model, //class
            config('project.table.editDelete'),
            function ($q) { //function exnending query
                return $q->selectRaw('IF (debit > 0,debit,credit) as amount')
                            ->with('files')
                            ->joinTable('accounts','account')->joinTable('transactions_categories','transactions_categories_id');
            },
            [ //filters array of callbacks
                'date_range' => function($q,$val) {
                    $query = $q->where('date','>=',$val['start']);
                    if ($val['end']) $query->where('date','<=',$val['end']);
                    return $query;
                },
                'import_batch_number' => function($q,$val) {
                    if (is_numeric($val)) {
                        return $q->where('import_batch_number','=',$val);
                    } else if ($val == 'l') {
                        return $q->where('import_batch_number','=',function($q) {
                            $q->select('import_batch_number')->from(with(new $this->model)->getTable())->orderBy('import_batch_number','desc')->first();
                        });
                    }
                    return $q;
                },
                'amount_min' => function($q,$val) {
                    return $q->where(function($q) use ($val) {
                        $q->where('debit','>=',$val)->orWhere('credit','>=',$val);
                    });
                },
                'amount_max' => function($q,$val) {
                    return $q->where(function ($q) use ($val) {
                        $q->where('debit','<=',$val)->orWhereNull('debit');
                    })
                    ->where(function($q) use ($val) {
                        $q->where('credit','<=',$val)->orWhereNull('credit');
                    });
                },
                'no_category' => function($q,$val) {
                    return $q->whereNull('transactions_categories_id');
                }
            ],
            false,  //custom search query
            function($q) { //extra data query
                return [
                    'debit'     => $q->sum('debit'),
                    'credit'    => $q->sum('credit')
                ];
            }
        );

        return response()->json( $response );
    }


    public function show(Request $request)
    {

        \User::checkAccess('accTransactions',['view','full']);
        
        $model          = $this->model::find($request->hash) ?? new $this->model;
        $modelsNvalues  = $model->buildFields()->getFieldModelValues();

        return response()->json(
            [
                'status'    => 'success',
                'hash'      => $request->hash,
                'forms'    => [
                    'main'  => [
                        'fields'    => $model->form_fields['main'],
                        'values'    => $modelsNvalues
                    ]
                    
                ]
            ],
            200
        );
    }

    public function store(Request $request) {

        \User::checkAccess('accTransactions','full');

        $model = $this->model::findOrMkNew($request->hash);

        $model->validateForm($request->forms['main']['values'])
              ->store([ 'hash' => $request->hash ], $request->forms['main']['values']);

    }

    public function importStep1 (Request $request) {

        return $this->process_import($save = false, $request);

    }

    public function importFinalStep (Request $request) {

        return $this->process_import($save = true, $request);

    }

    private function process_import($save = false, Request $request) {

        $form   = $request->forms['main']['values'];
        $status = 'success';
        $msg    = 'Uploaded';
        $data   = [];

        if (count($form['file']) == 0 || !$form['account']) {

            $status = 'error';
            $msg    = 'Select Account AND Import File';

        } else {

            $file = \FileModel::find($form['file'][0]);

            $res            = $this->model::orderBy('import_batch_number','desc')->first();
            $batch_num      = ($res && $res->import_batch_number ) ? $res->import_batch_number + 1 : 1;
            $account        = Account::find($form['account']);
            $account_hash   = $form['account'];
            $use_type       = $account->use_type;
            $account        = $account->name;

            

            $data = Excel::toCollection(new \User,storage_path('app/' . $file->path))[0]
                            ->transform(function($row) use ($batch_num,$account,$account_hash,$use_type) {
                                return [
                                    'date'                          => date('Y-m-d',strtotime($row[0])),
                                    'date_visual'                   => date('d M, Y',strtotime($row[0])),
                                    'description'                   => trim($row[1]),
                                    'debit'                         => $row[2],
                                    'credit'                        => $row[3] ?? null,
                                    'category_name'                 => null,
                                    'transactions_categories_id'    => null,
                                    'account_name'                  => $account,
                                    'account'                       => $account_hash,
                                    'use_type'                      => $use_type,
                                    'import_batch_number'           => $batch_num,
                                    'attention'                     => null,
                                    'approved'                      => null,
                                    'error'                         => false,
                                    'rules'                         => []
                                ];
                            });

            $data = $this->validate_import($data)->apply_import_rules($data);
            
            if ($save) {
                foreach ($data as $transaction) {
                    Transaction::create($transaction);
                }
                $data   = [];
                $msg    = 'Import Complete';
            }
            
        }

        return response()->json([ 'status' => $status, 'res' => $msg, 'data' => $data]);

    }

    protected function validate_import($transactions) {
        $msg = '';
        $matches = 0;

        foreach ($transactions as $key => $transaction) {
            $valid = true;
            $rowNum = $key + 1;
            
            if ((!$transaction['credit'] && !$transaction['debit']) || ($transaction['credit'] > 0 && $transaction['debit'] > 0)) {
                $valid = false;
                $msg = "Row #: {$rowNum} must have credit OR debit";
            } else if ($transaction['date'] == '1970-01-01') {
                $valid = false;
                $msg = "Row #: {$rowNum} invalid date";
                $msg .= implode('||',$transaction);
            } 

            if ($valid) {
                if ($this->model::where([
                        'date'          => $transaction['date'],
                        'account'       => $transaction['account'],
                        'debit'         => $transaction['debit'],
                        'credit'        => $transaction['credit'],
                        'description'   => $transaction['description']
                    ])->count()) {
                        if ($matches >= 3) {
                            $valid = false;
                            $msg .= "Multiple Transaction Matches Found";
                        }
                        $matches++;
                }
                
            }
            
            if (!$valid) {
                abort(
                    response()->json([ 'status' => 'error', 'res' => $msg ], 200)
                );
            }
        }

        return $this;
    }

    protected function apply_import_rules(Collection $data) {

        $rules = TransImportRule::all();

        $data->transform(function($transaction) use ($rules) {
            
            foreach ($rules as $rule) {
                
                $matched = true;
    
                ## DETERMINE IF TRANSACTION MATCHED THE RULE ##
                if ($rule->dates_range) {
                    if ($rule->dates_range['start'] && $transaction['date'] < $rule->dates_range['start']) $matched = false;
                    if ($matched && $rule->dates_range['end'] && $transaction['date'] > $rule->dates_range['end']) $matched = false;
                }
              
                if ($matched && $rule->description && $rule->description['string']) {
                    $preg = str_replace('*','\*',$rule->description['string']);
                    $preg = str_replace('/','\/',$preg);
                    switch ($rule->description['match_type']) {
                        case 'starts_with':
                            $matched = preg_match('/^'.$preg.'/i',$transaction['description']) == 1;
                        break;
                        case 'contains':
                            
                            $matched = preg_match('/'.$preg.'/i',$transaction['description']) == 1;
                        break;
                        case 'ends_with':
                            $matched = preg_match('/'.$preg.'$/i',$transaction['description']) == 1;
                        break;
                    }
                }
    
                if ($matched && $rule->amount) {
                    $amount = $transaction['credit'] > 0 ? $transaction['credit'] : $transaction['debit'];
                    if (!empty($rule->amount['min']) && $amount < $rule->amount['min']) $matched = false;
                    if (!empty($rule->amount['max']) && $amount > $rule->amount['max']) $matched = false;
                }

                if ($matched && $rule->debit_credit) {
                    $trans_type = $transaction['credit'] > 0 ? 'credit' : 'debit';
                    $matched = $rule->debit_credit == $trans_type;
                }
    
                ## APPLY RULE 
                if ($matched) {
                    if ($rule->transactions_categories_id) {
                        $trans_cat = TransCategory::find($rule->transactions_categories_id);
                        $transaction['transactions_categories_id']  = $rule->transactions_categories_id;
                        $transaction['category_name']               = $trans_cat->name;
                        $transaction['use_type']                    = $trans_cat->use_type;
                    }
                    if ($rule->attention) $transaction['attention'] = $rule->attention;
                    if ($rule->approved) $transaction['approved'] = $rule->approved;
                    $transaction['rules'][] = $rule->name;
                }  

            }
    
            return $transaction;

        });

        return $data;
        
    }

    public function update_category(Request $request) {

        $this->model::find($request->hash)->update(['transactions_categories_id' => $request->cat_id ]);

        return response()->json([ 'status' => 'success', 'res' => 'Category Updated']);
    }

    public function update_notes(Request $request) {

        $this->model::find($request->hash)->update(['notes' => $request->notes ]);

        return response()->json([ 'status' => 'success', 'res' => 'Notes Updated']);
    }

    public function update_approved(Request $request) {

        $this->model::find($request->hash)->update(['approved' => $request->approved ]);

        return response()->json([ 'status' => 'success', 'res' => 'Approved Updated']);
    }

    public function update_attention(Request $request) {

        $this->model::find($request->hash)->update(['attention' => $request->attention ]);

        return response()->json([ 'status' => 'success', 'res' => 'Approved Updated']);
    }

    public function bulk_delete(Request $request) {

        $this->model::whereIn('hash',$request->hashes)->delete();

        return response()->json([ 'status' => 'success', 'res' => count($request->hashes) . ' Transactions Deleted']);
    }

    public function bulk_approve(Request $request) {

        $this->model::whereIn('hash',$request->hashes)->update(['approved' => '1']);

        return response()->json([ 'status' => 'success', 'res' => count($request->hashes) . ' Transactions Approved']);
    }

    public function save_file_by_hash (Request $request) {
        
        \FileModel::find($request->receipt)->update([
            'attachment_id'    => $request->hash,
            'attachment_type'  => $this->model,
            'attachment_field' => 'receipt',
            'is_saved'         => true
        ]);

        return response()->json([ 'status' => 'success', 'res' => 'File Saved']);

    }

}
