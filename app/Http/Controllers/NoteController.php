<?php
namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NoteController extends Controller
{

    public function list(Request $request)
    {

        return Note::where([
            ['assignable_id',$request->form_hash],
            ['assignable_field',$request->field_name],
            ['assignable_type_slug',$request->slug]            
        ])->orderBy('created_at','desc')->get();

    }
}
