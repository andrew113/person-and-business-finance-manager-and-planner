<?php
namespace App\Models;

use Auth, Uuid;
use App\Models\OAD\OADModel;

class Phone extends OADModel
{
	protected $table = 'phone_numbers';
	protected $guarded = ['hash'];
	protected $primaryKey = 'hash';
    public $incrementing = false;
    protected $hidden = ['assignable_id', 'assignable_type', 'assignable_field', 'user_updated','user_created','created_at','updated_at'];

    public function assignable() {
        return $this->morphTo();
    }

    public function json_assignable() {
        return $this->morphMany(\App\Models\JsonTable::class, 'assignable')->orderBy('created_at','desc');
    }

	public static function boot() {
        parent::boot();

        self::creating(function($model) {

            $model->hash            = Uuid::generate()->string;
            $model->user_created    = app()->runningInConsole() ? '' : Auth::user()->hash;

        });

        self::updating(function($model) {

            $model->user_updated    = app()->runningInConsole() ? '' : Auth::user()->hash;

            //saving phone number update history
            if ($model->getOriginal('number') && $model->number) {

                $compare_by = ['hash','type','number','ext'];
                $original_vals = collect($model->getOriginal())->filter(function($item,$key) use ($compare_by) {
                    return in_array($key,$compare_by);
                })->toArray();
                
                //search history of saved number to find a match, create new record if no match has been found
                if ($model->json_assignable()->whereJsonContains( 'table_data', $original_vals )->count() == 0) {
                    $model->json_assignable()->create([
                        'user_updated'  => app()->runningInConsole() ? '' : Auth::user()->hash,
                        'user_created'  => app()->runningInConsole() ? '' : Auth::user()->hash,
                        'table_data'    => $original_vals,
                        ]);
                }

            }

        });

    }


}
