<?php

namespace App\Models;

use Field, Uuid;
use App\Models\OAD\OADModel;

class TransImportRule extends OADModel
{

	protected $table = 'transaction_import_rules';
	protected $primaryKey = 'hash';
	protected $guarded = ['hash'];

	public $form_fields = [];
    public $validated = false;
	public $form_errors = [];

	protected $casts = [
		'description' 	=> 'array',
		'dates_range' 	=> 'array',
		'amount' 		=> 'array',
    ];

	public function buildFields($return_form_fields = false) {

        $this->form_fields['main'] = $this->buildFormFields([
			 Field::init()->name('name')->label('Rule Name')->required()->toArray(),
			 Field::init()->name('description')->label('Description')->dValue(['match_type' => '', 'string' => ''])->toArray(),
			 Field::init()->name('amount')->label('Amount')->dValue(['min' => '', 'min' => ''])->toArray(),
			 Field::init()->name('debit_credit')->type('toggle')->label('Transaction Type')->options(['debit'=>'Debit','credit'=>'Credit'])->groupClass('w-100')->toArray(),
			 Field::init()->name('dates_range')->type('daterange')->dValue(['start' => '', 'end' => ''])->label('Withing Dates')->toArray(),
			 Field::init()->name('attention')->type('echeck')->label(false)->placeholder('Attention Flag')->toArray(),
			 Field::init()->name('approved')->type('echeck')->label(false)->placeholder('Auto Approve')->toArray(),
			 Field::init()->type('select')->name('transactions_categories_id')->ajax('/d/trans-categories/list')->allow_clear()->label('Assign The Category For This Rule')->toArray(),
		]);

		return $return_form_fields ? $this->form_fields[$return_form_fields] : $this;
		
	}
	
	public static function boot() {
        parent::boot();

        self::creating(function($model) {

			$model->hash = Uuid::generate()->string;

        });

    }

}
