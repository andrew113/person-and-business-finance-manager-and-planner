<?php

namespace App\Models;

use Uuid;
use App\Models\OAD\OADModel;

class JsonTable extends OADModel
{
	protected $table = 'json_table_assignable';
	protected $guarded = ['hash'];
	protected $primaryKey = 'hash';
    public $incrementing = false;

    protected $casts = [
		'table_data' 	=> 'array',
    ];

    public function assignable() {
        return $this->morphTo();
    }

	public static function boot() {
        parent::boot();

        self::creating(function($model) {

			$model->hash = Uuid::generate()->string;

        });

    }


}
