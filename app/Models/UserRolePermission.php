<?php

namespace App\Models;

use App\Models\OAD\OADModel;

class UserRolePermission extends OADModel
{
	protected $guarded = ['id'];
	public $timestamps = false;
	protected $table = 'users_roles_permissions';

}
