<?php

namespace App\Models;

use Field, Uuid;
use App\Rules\DebitCredit;
use App\Models\OAD\OADModel;


class Transaction extends OADModel
{
	protected $table = 'transactions';
	protected $primaryKey = 'hash';
	protected $guarded = ['hash'];

	public $form_fields = [];
    public $validated = false;
	public $form_errors = [];

	protected $use_type_options = [
		'personal'		=> 'Personal',
		'business'		=> 'Business',
	];

	public function buildFields($return_form_fields = false) {

        $this->form_fields['main'] = $this->buildFormFields([
			Field::init()->type('date')->name('date')->label('Date')->required()->toArray(),
			Field::init()->type('select')->name('account')->label('Account')->ajax('/d/accounts/list')->toArray(),
			Field::init()->type('select')->name('transactions_categories_id')->ajax('/d/trans-categories/list')->allow_clear()->label('Category')->toArray(),
			Field::init()->type('textarea')->name('description')->label('Transaction Description')->toArray(),
			Field::init()->type('number')->name('debit')->label('Debit (Withdraw)')->mask('$')->required([new DebitCredit()])->toArray(),
			Field::init()->type('number')->name('credit')->label('Credit (Deposite)')->mask('$')->required(['present','numeric','nullable'])->toArray(),
			Field::init()->type('text')->name('notes')->label('Notes')->toArray(),
			Field::init()->type('file')->name('receipt')->max_files(1)->label('Receipt')->toArray(),
			Field::init()->type('echeck')->name('approved')->label(false)->placeholder('Approved')->toArray(),
			Field::init()->type('echeck')->name('attention')->label(false)->placeholder('Attention')->toArray(),
			Field::init()->type('toggle')->name('use_type')->options($this->use_type_options)->label('Account Use')->toArray(),
        ]);

        return $return_form_fields ? $this->form_fields[$return_form_fields] : $this;
	}

	function scopeIsExpense($q, $expenseType = null) {

		return $q->whereIn('transactions_categories_id', function($q) use ($expenseType) {
				$q->select('hash')->from(with(new \App\Models\TransCategory())->getTable())
						->where('type','expense');
				if ($expenseType) $q->where('expense_category',$expenseType);
			});
	}
	function scopeIsIncome($q) {

		return $q->whereIn('transactions_categories_id', function($q) {
				$q->select('hash')->from(with(new \App\Models\TransCategory())->getTable())
						->where('type','income');
			});
	}
	function scopeApproved($q) {
		return $q->where('approved','1');
	}

    public function scopeMonthYear($query,$month,$year) {
        $query->whereMonth('date',$month )->whereYear('date',$year);
    }
	

	// operation type : business /personal /real estate
	function scopeOtype($q, $use_type = 'personal') {
		return $q->where('use_type',$use_type);
	}
	
	public static function boot() {
        parent::boot();

        self::creating(function($model) {

			$model->hash = Uuid::generate()->string;

        });

    }

}
