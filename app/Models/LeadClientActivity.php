<?php
namespace App\Models;

use Field, Uuid;
use App\Models\OAD\OADModel;

class LeadClientActivity extends OADModel
{
	protected $table = 'leads_clients_activity';
	protected $guarded = ['hash'];
	protected $primaryKey = 'hash';
    public $incrementing = false;


	public $form_fields = [];
    public $validated = false;
    public $form_errors = [];

	public function buildFields($return_fields = false, $field_group = '') {

		/* $this->form_fields = $this->buildFormFields([
             Field::init()->name('first_name')->label('First Name')->toArray(),
         ]);*/

        return $return_fields ? $this->form_fields : $this;
    }

	public function scopeList($query)
    {
        return $query;
    }

    public function scopeExportList($query) {
		return $query->select('hash');
	}

	public static function boot() {
        parent::boot();

        self::creating(function($model) {

			$model->hash = Uuid::generate()->string;

        });

    }


}
