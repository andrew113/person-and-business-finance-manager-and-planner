<?php

namespace App\Models;

use Field, Uuid;
use App\Models\OAD\OADModel;

class TransCategory extends OADModel
{

	protected $table = 'transactions_categories';
	protected $primaryKey = 'hash';
	protected $guarded = ['hash'];

	public $form_fields = [];
    public $validated = false;
	public $form_errors = [];

	protected $type_options = [
		'expense'	=> 'Expense',
		'income'	=> 'Income',
		'other'		=> 'Other'
	];
	protected $expense_category_options = [
		'variable'	=> 'Variable',
		'fixed'		=> 'Fixed'
	];
	protected $use_type_options = [
		'personal'		=> 'Personal',
		'business'		=> 'Business',
	];

	public function getTypeOptions() {
		return $this->type_options;
	}

	public function getExpenseCategoryOptions() {
		return $this->expense_category_options;
	}

	public function buildFields($return_form_fields = false) {

        $this->form_fields['main'] = $this->buildFormFields([
			Field::init()->name('name')->label('Name')->required()->toArray(),
			Field::init()->type('toggle')->name('type')->options($this->type_options)->label('Category Type')->required()->toArray(),
			Field::init()->type('toggle')->name('use_type')->options($this->use_type_options)->label('Usage')->required()->toArray(),
			Field::init()->type('toggle')->name('expense_category')->options($this->expense_category_options)->label('Expense Type')->required('required_if:type,expense')->toArray()
        ]);

        return $return_form_fields ? $this->form_fields[$return_form_fields] : $this;
	}

	public function scopeExpenses($q) {
		return $q->where('type','expense');
	}
	
	public static function boot() {
        parent::boot();

        self::creating(function($model) {

			$model->hash = Uuid::generate()->string;

        });

    }

}
