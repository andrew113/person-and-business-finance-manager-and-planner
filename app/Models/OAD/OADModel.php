<?php

namespace App\Models\OAD;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use App\Models\OAD\File;

class OADModel extends Model {

    public $form_fields = [];
    public $forms = [
        'main' => [
            'name'      => 'main',
            'primary'   => 'hash'
        ]
    ];

    protected $validated = false;
    protected $form_errors = [];
    protected $guarded = ['hash'];
    public $incrementing = false;

    public function scopeJoinUser($query,$join_on_field) {
        $this_table = with(new $this)->getTable();
        $query->leftJoin('users' , $this_table . '.' . $join_on_field, '=', 'users.hash');
    }
    
    public function scopeJoinTable($query,$join_table,$using_column,$joint_table_column = 'hash') {
        $this_table = with(new $this)->getTable();
        $query->leftJoin($join_table , $this_table . '.' . $using_column, '=', $join_table.'.'.$joint_table_column );
    }
    public function getFormsModelValues() {
        $forms = [];
        foreach($this->forms as $form) {
            $forms[$form['name']] = $this->getFieldModelValues($form['name'],$form['primary']);
        }
        return $forms;
    }

    public function files() {
        return $this->morphMany(File::class, 'attachment');
    }
    public function addresses() {
        return $this->morphMany(\App\Models\Address::class, 'assignable');
    }
    public function phone_numbers() {
        return $this->morphMany(\App\Models\Phone::class, 'assignable');
    }
    public function emails() {
        return $this->morphMany(\App\Models\Email::class, 'assignable');
    }
    public function notes_list() {
        return $this->morphMany(\App\Models\Note::class, 'assignable');
    }
    
    public function getAssignedAddressAttribute() {
        $res = $this->addresses()->where('assignable_field','address')->first();
        return $res ?? [];
    }
    public function getAssignedPhoneAttribute() {
        $res = $this->phone_numbers()->where('assignable_field','phone')->first();
        return $res ?? [];
    }
    public function getAssignedFaxAttribute() {
        $res = $this->phone_numbers()->where('assignable_field','fax')->first();
        return $res ?? [];
    }
    public function getAssignedEmailAttribute() {
        $res = $this->emails()->where('assignable_field','email')->first();
        return $res ?? [];
    }

    public function prepareAssignableSelectors($fields = [],$data = []) {
        $selectors = [];
        foreach ($fields as $field_name)
            $selectors[$field_name] = [ 'hash' => !empty($data[$field_name]['hash']) ? $data[$field_name]['hash'] : '' ];
        return $selectors;
    }

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    public function buildFormFields($fields = []) {
        $arr = [];

        foreach ($fields as $field) $arr[$field['key']] = $field;

        return $arr;
    }

    public static function findOrMkNew($hash)
    {
        $obj = static::find($hash);
        return $obj ?: new static;
    }

    public function getFieldModelValues($form_name = 'main', $primaryKey = 'hash') {
        $return         = [];
        $fields_data    = $this->buildFields($form_name);
        
        foreach ($fields_data  as $field) {
            
            $objKey = $field['db_name'];
            
            if ($this->$primaryKey) { //existing record

                switch ($field['type']) {
                    case "file":
                        $files_arr = [];
                        for ($i = 0; $i < $this->files->count(); $i ++) {
                            if ($this->files[$i]->is_saved && $this->files[$i]->attachment_field == $objKey) {
                                $files_arr[] = [
                                    'hash'  => $this->files[$i]->hash,
                                    'name'  => $this->files[$i]->file_name
                                ];
                            }
                        }
                        $return[$objKey]= $files_arr;  
                    break;
                    case "address":
                    case "phone":
                    case "email":
                    case "fax":
                        $attr = 'assigned_' . $field['type'];
                        $return[$objKey] = $this->$attr;
                    break;
                    default:
                    $return[$objKey] = $field['assignVal'] ? $this->$objKey : '';
                }

            } else { //new record
                $return[$objKey] = $field['dValue']!=='' ? $field['dValue'] : '';
            }
        }

        return $return;
    }

    public function validateAllForms($forms_data) {

        $last_key = array_key_last($this->forms);

        foreach ($this->forms as $key => $form) {
            $this->validateForm(
                $forms_data[$form['name']]['values'] ?? [],
                $form['name'],
                $key == $last_key //only return response on the last form
            );
        }
        return $this;
    }

    public function validateForm( $data, $form_name = 'main', $return_response = true, $rules = [], $attributes = [], $messages = []) {

        $fields_data = $this->buildFields($form_name);

        $model_rules = [];
        foreach ($fields_data as $field) {
            if ($field['required']) {
                $model_rules[$field['name']] = $field['required'];
            }
        }

        $rules = array_merge($model_rules,$rules);
        $validator = Validator::make($data, $rules);

        $model_attributes = [];
        foreach ($fields_data as $field) {
            $model_attributes[$field['name']] = $field['label'] ? $field['label'] : $field['placeholder']; //placeholder for checkboxes
        }
        $attributes = array_merge($model_attributes,$attributes);
        $validator->setAttributeNames($attributes);

        $validator->setCustomMessages(count($messages) ? $messages : ['required' => ':attribute is required']);

        $this->validated = !$validator->fails();

        if (!$this->validated) {
            $this->form_errors = array_merge($this->form_errors,$validator->errors()->all());
        }

        if (count($this->form_errors) && $return_response) {
            abort(
                response()->json([ 'status' => 'error', 'res' => implode('<br>',$this->form_errors) ], 200)
            );
        }

        return $this;

    }

    public function saveAllForms($forms_selector_values = [], $data) {

        foreach ($this->forms as $form_name => $form) {
            $primaryKey     = $this->forms[$form_name]['primary'];
            //so we can pass custom primary key values to the function, 
            //if there no value is passed through this variable we are going to look for the value inside the form
            $primaryValue   = array_key_exists($form_name,$forms_selector_values) ? $forms_selector_values[$form_name] : $data[$form_name]['values'][$primaryKey];
            $msg = $form_name == 'main' ? 'Saved' : false;
            $this->store( [ $primaryKey => $primaryValue ] , $form, $msg, $form_name );
        }
    }

    public function store($selector, $data, $successMsg = 'Saved', $form_name = 'main') {

        $primaryKey     = $this->primaryKey;
        $fields_data    = $this->buildFields($form_name);        

        try {

            $res = $this->updateOrCreate($selector, $data);

        } catch (Exception $e) {

            abort(
                response()->json([ 'status' => 'error', 'res' => $e->getMessage() ], 200)
            );

        }

        foreach ($fields_data as $field_data) {
            $model = null;
            switch ($field_data['type']) {
                case "file":

                    $files_hashes   = [];
                    if (is_array($data[$field_data['name']])) {
                        foreach ($data[$field_data['name']] as $file_hash) {
                            $files_hashes[] = $file_hash;
                            //update tmp file to status saved
                            File::whereIn('hash',$file_hash)->update([
                                'attachment_id'    => $res->$primaryKey,
                                'attachment_type'  => get_class($this),
                                'attachment_field'  => $field_data['db_name'],
                                'is_saved'         => true
                            ]);
                        }
                    }                    
                    
                    //delete removed files
                    File::where([
                        'attachment_id' => $res->$primaryKey,
                        'attachment_type' => get_class($this)
                    ])
                    ->whereNotIn('hash',$files_hashes)
                    ->update(['is_saved' => 0 ]);
                
                break;

            }
        }

        
        
        if ($successMsg) {

            abort(
                
                response()->json([ 
                    'status' => 'success',
                    'obj'       => $res,
                    'hash'      => $res->$primaryKey, 
                    'res'       => $successMsg ], 
                200)
            );

        } else {

            return $res;

        }

    }

}
