<?php

namespace App\Models\OAD;

use Illuminate\Database\Eloquent\Model;

class UserSession extends Model
{
    protected $table = 'users_sessions';
    protected $guarded = [];

    protected $casts = [
        'session_info' => 'json'
    ];
}
