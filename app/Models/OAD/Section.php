<?php

namespace App\Models\OAD;

class Section extends OADModel
{
    protected $table = 'sections';
    protected $guarded = ['id'];
    protected $hidden = ['parent_id','slug','vue_router_id'];

	public function routes() {
        return $this->belongsTo('App\Models\OAD\Router', 'vue_router_id');
    }

}
