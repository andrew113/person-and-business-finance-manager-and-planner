<?php

namespace App\Models;

use Field, Uuid;
use App\Models\OAD\OADModel;


class Account extends OADModel
{

	protected $table 		= 'accounts';
	protected $primaryKey 	= 'hash';
	protected $guarded 		= ['hash'];
	public $timestamps 		= false;

	public $form_fields = [];
    public $validated = false;
	public $form_errors = [];

	protected $type_options = [
		'bank'			=> 'Bank Account',
		'credit_line'	=> 'Credit Line',
		'credit_card'	=> 'Credit Card',
		'cash'			=> 'Cash'
	];
	protected $use_type_options = [
		'personal'		=> 'Personal',
		'business'		=> 'Business',
	];

	public function getTypeOptions() {
		return $this->type_options;
	}
	public function getUseTypeOptions() {
		return $this->use_type_options;
	}


	public function buildFields($return_form_fields = false) {

        $this->form_fields['main'] = $this->buildFormFields([
			Field::init()->name('name')->label('Name')->required()->toArray(),
			Field::init()->name('institution')->label('Institution')->toArray(),
			Field::init()->name('number')->label('Account Number')->toArray(),
			Field::init()->type('select')->name('type')->options($this->type_options)->required()->label('Account Type')->toArray(),
			Field::init()->type('toggle')->name('use_type')->options($this->use_type_options)->label('Account Use')->toArray(),

        ]);

        return $return_form_fields ? $this->form_fields[$return_form_fields] : $this;
	}
	
	public static function boot() {
        parent::boot();

        self::creating(function($model) {

			$model->hash = Uuid::generate()->string;

        });

    }

}
