<?php

namespace App\Models;

use Field, Uuid;
use App\Models\OAD\OADModel;


class Tax extends OADModel
{
	protected $table = 'taxes';
	protected $primaryKey = 'hash';
	protected $guarded = ['hash'];
	public $timestamps = false;

	public $form_fields = [];
    public $validated = false;
	public $form_errors = [];

	public function buildFields($return_form_fields = false) {

        $this->form_fields['main'] = $this->buildFormFields([
			Field::init()->name('name')->label('Name')->required()->toArray(),
			Field::init()->type('number')->name('percent')->label('Amount')->mask('%')->required('numeric')->toArray()
        ]);

        return $return_form_fields ? $this->form_fields[$return_form_fields] : $this;
	}
	
	public static function boot() {
        parent::boot();

        self::creating(function($model) {

			$model->hash = Uuid::generate()->string;

        });

    }

}
