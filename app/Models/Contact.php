<?php
namespace App\Models;

use App\Models\OAD\Field as OADField;
use Field, Uuid;
use App\Models\OAD\OADModel;
use App\Traits\ModelHelper;

class Contact extends OADModel
{
    use ModelHelper;

	protected $table = 'contacts';
    protected $guarded = ['hash'];
	protected $primaryKey = 'hash';
    public $incrementing = false;

	public $form_fields = [];
    public $form_errors = [];

    public $gender_options = [
        'male'      => 'Male',
        'female'    => 'Female'
    ];

	public function buildFields($return_form_fields = false) {
        
		$this->form_fields['main'] = $this->buildFormFields([
            Field::init()->name('gender')->type('select')->label('Gender')->options($this->gender_options)->allow_clear()->toArray(),
            Field::init()->name('first_name')->label('First Name')->required()->toArray(),
            Field::init()->name('last_name')->label('Last Name')->toArray(),
            Field::init()->name('middle_name')->label('Middle Name')->toArray(),
            Field::init()->name('birthday')->type('date')->label('Birthday')->toArray(),
            Field::init()->name('address')->type('address')->toArray(),
            Field::init()->name('phone')->type('phone')->toArray(),
            Field::init()->name('fax')->type('fax')->toArray(),
            Field::init()->name('email')->type('email')->toArray(),
            Field::init()->name('notes')->type('notes')->toArray(),
        ]);
        
        return $return_form_fields ? $this->form_fields[$return_form_fields] : $this;

    }

	public function scopeList($query)
    {
        return $query->addSelect(\DB::raw('CONCAT_WS(" ", first_name, last_name) as full_name'))->with('addresses','phone_numbers','emails');
    }

    public function scopeExportList($query) {
		return $query->select('hash');
    }

    public function getFullNameAttribute()
	{
	    return "{$this->first_name} {$this->last_name}";
    }

	public static function boot() {
        parent::boot();

        self::creating(function($model) {
            $model->hash = Uuid::generate()->string;
        });

        // before delete() method call this
        static::deleting(function($contact) { 
            $contact->addresses()->delete();
            $contact->phone_numbers()->delete();
            $contact->emails()->delete();
       });

    }
    
    public function saveContact($hash, $data, $successMsg = 'Saved' ) {
        
        $contact    = $this->updateOrCreate( [ 'hash' => $hash ], $data);
        $selectors  = $this->prepareAssignableSelectors(['address','phone','fax','email'],$data);

        $address    = $contact->addresses()->updateOrCreate($selectors['address'],array_merge($data['address'],['assignable_field' => 'address']));
        $email      = $contact->emails()->updateOrCreate($selectors['email'],array_merge($data['email'],['assignable_field' => 'email']));
        $phone      = $contact->phone_numbers()->updateOrCreate($selectors['phone'],array_merge($data['phone'],['assignable_field' => 'phone']));
        $fax        = $contact->phone_numbers()->updateOrCreate($selectors['fax'],array_merge($data['fax'],['assignable_field' => 'fax']));
        $contact->notes_list()->create(array_merge(['text' => $data['notes'],'assignable_field' => 'notes']));

        abort(
            response()->json([ 
                'status'        => 'success',
                'address_hash'  => $address->hash,
                'email_hash'    => $email->hash,
                'phone_hash'    => $phone->hash,
                'fax_hash'      => $fax->hash,
                'obj'           => $contact,
                'hash'          => $contact->hash, 
                'res'           => $successMsg 
            ], 
            200)
        );

    }
}
