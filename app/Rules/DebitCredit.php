<?php

namespace App\Rules;

use Request;
use Illuminate\Contracts\Validation\Rule;

class DebitCredit implements Rule
{
    private $msg = '';
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $fields = Request::input('forms.main.values');
        
        if (!$fields['credit'] && !$fields['debit']) {
            $this->msg = 'Credit OR Debit is required';
            return false;
        } else if ($fields['debit'] > 0 && $fields['credit'] > 0) {
            $this->msg = "Can't enter both Credit AND Debit";
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->msg;
    }
}
