<?php
namespace App\Traits;

use Request;

trait TableHelpers {

    //generate response for vuetable query
    public function listBuilder($model = '', $actions = [], $queryExtention = false, $customFilters = [], $customSearch = false, $extraFunc = false) {

        $request = Request::input('params');

        list($sort_by,$sort_dir)    = explode("|",$request['sort']);
        $page                       = $request['page'];
        $per_page                   = $request['per_page'];
        $skip                       = ($page - 1) * $per_page;
        $search                     = $request['search'];
        $filters                    = $request['filters'];
        $table                      = $request['config'];
        $cols                       = [];
        $selectCols                 = [];
        $model_table                = $model::getTableName();
        $extra_data                 = '';

        //preparing columns to select from db
        foreach ($table['cols'] as $column) {
            if (!isset($column['noSelect'])) {
                $selectCols[] = isset($column['db']) ? $column['db'] . ' as ' .$column['name'] : $model_table.'.'.$column['name'];
                $cols[$column['name']] = isset($column['db']) ? $column['db'] : $model_table.'.'.$column['name'];
            }
        }
        $query = $model::select($selectCols);

        if ($queryExtention && is_callable($queryExtention)) {
            $query = call_user_func($queryExtention,$query);
        }       

        if ($search) { //go through all searchable queries
            $query->where(function($q) use ($table,$search,$model_table,$customSearch) {
    			foreach ($table['cols'] as $column) {
    				if (!(isset($column['searchable']) && $column['searchable'] === false)) {
                        $searchBy = !empty($column['searchBy']) ? $column['searchBy'] : $column['db'] ?? $column['name'];
                        if (strstr($searchBy,'.')) {
                            list($table,$field) = explode('.',$searchBy);
                        } else {
                            $table = $model_table;
                            $field = $searchBy;
                        }
    					$q->orWhere($table.'.'.$field,'LIKE','%' . $search . '%');
    				}
                }
                if ($customSearch) {
                    $q = call_user_func($customSearch,$q,$search);
                }
            });
		}

        if ($filters && is_array($filters)) {

            foreach ($filters as $field => $value) {
                if ($value) {
                    if (array_key_exists($field, $customFilters)) {
                        $query = call_user_func($customFilters[$field],$query,$value);
                    } else {
                        if ($value) {
                            $query->where($field,'LIKE','%' . $value . '%');
                        }
                    }
                }
            }

        }
        if ($extraFunc && is_callable($extraFunc)) {
            $extra_data = call_user_func($extraFunc,$query);
        } 
        $total      = $query->count();
        $last_page  = ceil($total / $per_page);
        $data       = $query->orderBy($sort_by, $sort_dir)->skip($skip)->take($per_page)->get();
        $from       = $skip + 1;
        $to         = $total < ($page * $per_page) ? $total : $page * $per_page;

        $data = $data->transform(function($item) use ($actions) {

            $return    = $item->getAttributes();
            $relations = $item->getRelations();

            foreach ($relations as $key => $relation) {
                $return[$key] = $relation;
            }

            if (!empty($actions)) $return['actions'] = $actions; 
            
            return $return;

        })->toArray();

        return [
            'pagination' => [
                'total'             => $total,
                'per_page'          => $per_page,
                'current_page'      => (int)$page,
                'last_page'         => $last_page,
                'next_page_url'     => '...',
                'prev_page_url'     => '...',
                'from'              => $from,
                'to'                => $to,
            ],
            'data'                  => $data,
            'extraData'             => $extra_data
        ];
    }

}
