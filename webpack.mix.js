const mix = require('laravel-mix');
var path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        '@': path.resolve(__dirname, 'resources/js')
      },
    },
  })
  
mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();

if (mix.inProduction()) {
  mix.version()
  mix.webpackConfig({
    output: {
      chunkFilename: 'js/chunks/[name].[chunkhash].js',
    }
  });
}
else {
  mix.webpackConfig({
    output: {
      chunkFilename: 'js/chunks/[name].js',
    }
  });
}
