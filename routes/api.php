<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


$routes = [
    'notes' => [
       'type'  => 'single',
       'controller'    =>'NoteController'
   ],
    'adjacent' => [
       'type'  => 'single',
       'controller'    =>'JsonTableController'
   ],
    'contacts' => [
       'type'  => 'resource',
       'controller'    =>'ContactController'
   ],
    'leadsclients' => [
       'type'  => 'resource',
       'controller'    =>'LeadClientController'
    ],
    'dashboard'         => [
        'type'  => 'single',
        'controller'    => 'DashboardController'
    ],
    'trans-import-rules'         => [
        'type'          => 'resource',
        'controller'    => 'TransImportRuleController'
    ],
    'taxes'         => [
        'type'  => 'resource',
        'controller'    => 'TaxController'
    ],
    'users'         => [
        'type'  => 'resource',
        'controller'    => 'Admin\UserController'
    ],
    'user-roles'         => [
        'type'  => 'resource',
        'controller'    => 'Admin\UserRoleController'
    ],
    'accounts'         => [
        'type'  => 'resource',
        'controller'    => 'AccountController'
    ],
    'transactions'         => [
        'type'  => 'resource',
        'controller'    => 'TransactionController'
    ],
    'trans-categories'         => [
        'type'  => 'resource',
        'controller'    => 'TransCatController'
    ]
];

Route::middleware(['auth:sanctum','OADSOFT\SPA\Http\Middleware\OADAuth'])->group(function() use ($routes) {

    //allows to call additional methods for the resource controllers
    Route::any('/d/{routeKey}/{method}', function($routeKey,$method) use ($routes) {
        return App::call('App\Http\Controllers\\' . $routes[$routeKey]['controller'] . '@' . $method);
    });

    foreach ($routes as $route => $data) {

        if ($data['type'] == 'resource') {

            Route::post($route, $data['controller'] . '@index');
            Route::get($route, $data['controller'] . '@show');
            Route::get($route . '/list', $data['controller'] . '@list');
            Route::post($route . '/save', $data['controller'] . '@store');
            Route::delete($route . '/{id}', $data['controller'] . '@destroy')->where('id', '([a-zA-Z0-9\-]+)');
            Route::post($route . '/export', $data['controller'] . '@export');
        }

    }

});