<?php

use Illuminate\Database\Seeder;

class SectionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sections')->delete();
        
        \DB::table('sections')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => NULL,
                'text' => 'Admin',
                'vue_router_id' => '3',
                'slug' => NULL,
                'cssClass' => 'fal fa-cogs',
                'type' => 'menu',
                'access_options' => 'none,view',
                'sort_order' => 100,
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => 1,
                'text' => 'Users',
                'vue_router_id' => '1',
                'slug' => 'users',
                'cssClass' => '',
                'type' => 'menu',
                'access_options' => 'none,view',
                'sort_order' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => 1,
                'text' => 'Roles',
                'vue_router_id' => '2',
                'slug' => 'usersRoles',
                'cssClass' => NULL,
                'type' => 'menu',
                'access_options' => 'none,full',
                'sort_order' => 2,
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => NULL,
                'text' => 'Dashboard',
                'vue_router_id' => '4',
                'slug' => 'dashboard',
                'cssClass' => 'fal fa-chart-bar',
                'type' => 'menu',
                'access_options' => 'view',
                'sort_order' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => NULL,
                'text' => 'Accounting',
                'vue_router_id' => '5',
                'slug' => NULL,
                'cssClass' => 'fad fa-abacus',
                'type' => 'menu',
                'access_options' => 'none,view',
                'sort_order' => 5,
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => 5,
                'text' => 'Transactions',
                'vue_router_id' => '6',
                'slug' => 'accTransactions',
                'cssClass' => NULL,
                'type' => 'menu',
                'access_options' => 'none,view,full',
                'sort_order' => 1,
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => 5,
                'text' => 'Accounts',
                'vue_router_id' => '7',
                'slug' => 'accAccounts',
                'cssClass' => NULL,
                'type' => 'menu',
                'access_options' => 'none,view,full',
                'sort_order' => 5,
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => 5,
                'text' => 'Trans. Categories',
                'vue_router_id' => '8',
                'slug' => 'accTransCats',
                'cssClass' => NULL,
                'type' => 'menu',
                'access_options' => 'none,view,full',
                'sort_order' => 10,
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => 5,
                'text' => 'Taxes',
                'vue_router_id' => '9',
                'slug' => 'taxes',
                'cssClass' => NULL,
                'type' => 'menu',
                'access_options' => 'none,view,full',
                'sort_order' => 15,
            ),
            9 => 
            array (
                'id' => 10,
                'parent_id' => 5,
                'text' => 'Import Rules',
                'vue_router_id' => '10',
                'slug' => 'transImportRules',
                'cssClass' => NULL,
                'type' => 'menu',
                'access_options' => 'none,view,full',
                'sort_order' => 20,
            ),
            10 => 
            array (
                'id' => 11,
                'parent_id' => NULL,
                'text' => 'Sales',
                'vue_router_id' => '11',
                'slug' => NULL,
                'cssClass' => 'fal fa-sack-dollar',
                'type' => 'menu',
                'access_options' => 'none,view',
                'sort_order' => 25,
            ),
            11 => 
            array (
                'id' => 12,
                'parent_id' => 11,
                'text' => 'Invoices',
                'vue_router_id' => '12',
                'slug' => 'invoices',
                'cssClass' => NULL,
                'type' => 'menu',
                'access_options' => 'none,view,full',
                'sort_order' => 5,
            ),
            12 => 
            array (
                'id' => 13,
                'parent_id' => 11,
                'text' => 'Subscriptions',
                'vue_router_id' => '13',
                'slug' => 'accSubscriptions',
                'cssClass' => NULL,
                'type' => 'menu',
                'access_options' => 'none,view,full',
                'sort_order' => 10,
            ),
            13 => 
            array (
                'id' => 14,
                'parent_id' => 11,
                'text' => 'Leads',
                'vue_router_id' => '14',
                'slug' => 'Leads',
                'cssClass' => NULL,
                'type' => 'menu',
                'access_options' => 'none,view,full',
                'sort_order' => 15,
            ),
            14 => 
            array (
                'id' => 15,
                'parent_id' => 11,
                'text' => 'Contacts',
                'vue_router_id' => '15',
                'slug' => 'contacts',
                'cssClass' => NULL,
                'type' => 'menu',
                'access_options' => 'none,view,full',
                'sort_order' => 20,
            ),
        ));
        
        
    }
}