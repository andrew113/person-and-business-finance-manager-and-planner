<?php

use Illuminate\Database\Seeder;

class TaxesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('taxes')->delete();
        
        \DB::table('taxes')->insert(array (
            0 => 
            array (
                'hash' => '5bdb2130-e3fc-11ea-a9fd-bfb66079c9fe',
                'name' => 'HST',
                'percent' => 13.0,
            ),
        ));
        
        
    }
}