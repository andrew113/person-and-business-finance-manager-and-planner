<?php

use Illuminate\Database\Seeder;

class TransactionsCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions_categories')->delete();
        
        \DB::table('transactions_categories')->insert(array (
            0 => 
            array (
                'hash' => '185003a0-eaf5-11ea-b5a2-350504b80f1b',
                'name' => 'Deposit',
                'type' => 'income',
                'use_type' => 'business',
                'expense_category' => NULL,
                'chart_color' => '#db4e12',
                'created_at' => '2020-08-30 19:14:58',
                'updated_at' => '2020-08-31 18:25:13',
            ),
            1 => 
            array (
                'hash' => '2d7e1f70-ebec-11ea-b960-b3da714a766e',
                'name' => 'Communication',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'fixed',
                'chart_color' => NULL,
                'created_at' => '2020-09-01 00:43:39',
                'updated_at' => '2020-09-01 00:43:39',
            ),
            2 => 
            array (
                'hash' => '503294b0-ebe8-11ea-b0ef-13014a5132e8',
                'name' => 'Credit Card Payment',
                'type' => 'other',
                'use_type' => 'business',
                'expense_category' => NULL,
                'chart_color' => NULL,
                'created_at' => '2020-09-01 00:15:59',
                'updated_at' => '2020-10-08 21:28:46',
            ),
            3 => 
            array (
                'hash' => '5aa8cc20-0999-11eb-bc0f-c36e8a3e0f78',
                'name' => 'Owner Investement in the Company',
                'type' => 'other',
                'use_type' => 'business',
                'expense_category' => NULL,
                'chart_color' => NULL,
                'created_at' => '2020-10-08 19:06:22',
                'updated_at' => '2020-10-08 19:06:22',
            ),
            4 => 
            array (
                'hash' => '5f5ba4d0-ebed-11ea-bc6c-b9a128b56ba3',
                'name' => 'Meals, Gifts & Entertainment',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => NULL,
                'created_at' => '2020-09-01 00:52:12',
                'updated_at' => '2020-10-08 19:00:20',
            ),
            5 => 
            array (
                'hash' => '63e22f20-e3fc-11ea-aae1-f994b67e1810',
                'name' => 'Office Rent',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'fixed',
                'chart_color' => '#db3f0a',
                'created_at' => '2020-08-21 22:19:33',
                'updated_at' => '2020-10-08 19:07:53',
            ),
            6 => 
            array (
                'hash' => '6aa519e0-e3fc-11ea-9542-2d50c471d231',
                'name' => 'Marketing Material',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => '#f6ff3b',
                'created_at' => '2020-08-21 22:19:44',
                'updated_at' => '2020-08-31 18:54:36',
            ),
            7 => 
            array (
                'hash' => '6c0650e0-ebec-11ea-a9bf-e946eab9f32d',
                'name' => 'Travel',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => NULL,
                'created_at' => '2020-09-01 00:45:24',
                'updated_at' => '2020-09-01 00:45:24',
            ),
            8 => 
            array (
                'hash' => '74765f50-e3fc-11ea-9826-5b44b8b5ccde',
                'name' => 'Subcontractors',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => '#d609df',
                'created_at' => '2020-08-21 22:20:01',
                'updated_at' => '2020-08-31 20:30:53',
            ),
            9 => 
            array (
                'hash' => '75778900-0999-11eb-958d-45ba70374f1f',
                'name' => 'Personal Withdraw',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => NULL,
                'created_at' => '2020-10-08 19:07:07',
                'updated_at' => '2020-10-08 19:07:07',
            ),
            10 => 
            array (
                'hash' => '8313c660-0999-11eb-b558-1b62661be832',
                'name' => 'Refund',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => NULL,
                'created_at' => '2020-10-08 19:07:29',
                'updated_at' => '2020-10-08 19:07:29',
            ),
            11 => 
            array (
                'hash' => '843a1570-e3fc-11ea-bae3-e529d4dccd05',
                'name' => 'Bank Charges',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => NULL,
                'created_at' => '2020-08-21 22:20:27',
                'updated_at' => '2020-08-21 22:20:27',
            ),
            12 => 
            array (
                'hash' => '99f9ee20-ebe8-11ea-a755-15623c2b7da1',
                'name' => 'Advertising Budget',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => NULL,
                'created_at' => '2020-09-01 00:18:03',
                'updated_at' => '2020-10-08 19:00:37',
            ),
            13 => 
            array (
                'hash' => 'a07b8bf0-0999-11eb-a33a-a1777c505051',
                'name' => 'CPP Payment',
                'type' => 'other',
                'use_type' => 'business',
                'expense_category' => NULL,
                'chart_color' => NULL,
                'created_at' => '2020-10-08 19:08:19',
                'updated_at' => '2020-10-08 19:08:19',
            ),
            14 => 
            array (
                'hash' => 'aa464c50-ebe9-11ea-9bbd-4df05a0211cb',
                'name' => 'Office Supplies',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => NULL,
                'created_at' => '2020-09-01 00:25:40',
                'updated_at' => '2020-09-01 00:25:40',
            ),
            15 => 
            array (
                'hash' => 'aab77f40-0998-11eb-97e8-a138ae5aa1a3',
                'name' => 'Insurance',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'fixed',
                'chart_color' => NULL,
                'created_at' => '2020-10-08 19:01:26',
                'updated_at' => '2020-10-08 19:01:26',
            ),
            16 => 
            array (
                'hash' => 'ab92cfd0-0999-11eb-9ee4-a103d7817f49',
                'name' => 'HST Payment',
                'type' => 'other',
                'use_type' => 'business',
                'expense_category' => NULL,
                'chart_color' => NULL,
                'created_at' => '2020-10-08 19:08:37',
                'updated_at' => '2020-10-08 19:08:37',
            ),
            17 => 
            array (
                'hash' => 'c7d8ae00-ebea-11ea-ad97-a58f4548aa9f',
                'name' => 'Clients Budget',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => NULL,
                'created_at' => '2020-09-01 00:33:39',
                'updated_at' => '2020-09-01 00:33:39',
            ),
            18 => 
            array (
                'hash' => 'cccc5bb0-ebec-11ea-a992-01c2c8f49c1c',
                'name' => 'Software',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'variable',
                'chart_color' => NULL,
                'created_at' => '2020-09-01 00:48:06',
                'updated_at' => '2020-09-01 00:48:06',
            ),
            19 => 
            array (
                'hash' => 'e90b34a0-ebe8-11ea-9997-f3e4a95635ca',
                'name' => 'Software Subscription',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'fixed',
                'chart_color' => NULL,
                'created_at' => '2020-09-01 00:20:16',
                'updated_at' => '2020-09-01 00:29:35',
            ),
            20 => 
            array (
                'hash' => 'fa28daf0-ebe9-11ea-97b3-c3c8938aba23',
                'name' => 'Domains, Hosting, Servers',
                'type' => 'expense',
                'use_type' => 'business',
                'expense_category' => 'fixed',
                'chart_color' => NULL,
                'created_at' => '2020-09-01 00:27:54',
                'updated_at' => '2020-10-08 19:00:58',
            ),
        ));
        
        
    }
}