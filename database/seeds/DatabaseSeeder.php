<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SectionsTableSeeder::class);
        $this->call(VueRouterTableSeeder::class);
        $this->call(TransactionsCategoriesTableSeeder::class);
        $this->call(TaxesTableSeeder::class);
        $this->call(AccountsTableSeeder::class);
        $this->call(TransactionsTableSeeder::class);
        $this->call(TransactionImportRulesTableSeeder::class);
    }
}
