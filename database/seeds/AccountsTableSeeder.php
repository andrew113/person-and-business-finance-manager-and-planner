<?php

use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('accounts')->delete();
        
        \DB::table('accounts')->insert(array (
            0 => 
            array (
                'hash' => '4bea0fc0-e3fc-11ea-8218-97b688fd9b8d',
                'name' => 'TD Business Chequing',
                'institution' => 'TD',
                'number' => '4535',
                'type' => 'bank',
                'use_type' => 'business',
            ),
            1 => 
            array (
                'hash' => '54dc53e0-e3fc-11ea-a5a8-0decb75b46da',
                'name' => 'TD CC Business',
                'institution' => 'TD',
                'number' => '7482',
                'type' => 'credit_card',
                'use_type' => 'business',
            ),
            2 => 
            array (
                'hash' => 'a10d6e60-0996-11eb-b9b3-4b71d4625d57',
                'name' => 'TD Personal Chequing',
                'institution' => 'TD',
                'number' => '7551',
                'type' => 'bank',
                'use_type' => 'personal',
            ),
        ));
        
        
    }
}