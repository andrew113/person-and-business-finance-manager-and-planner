<?php

use Illuminate\Database\Seeder;

class TransactionImportRulesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transaction_import_rules')->delete();
        
        \DB::table('transaction_import_rules')->insert(array (
            0 => 
            array (
                'hash' => '03155730-09ad-11eb-890e-c735a674948d',
                'name' => 'CPP Payment',
                'description' => '{"string": "EMPTX", "match_type": "starts_with"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'debit',
                'transactions_categories_id' => 'a07b8bf0-0999-11eb-a33a-a1777c505051',
                'attention' => NULL,
                'approved' => NULL,
                'created_at' => '2020-10-08 21:27:05',
                'updated_at' => '2020-10-08 21:27:05',
            ),
            1 => 
            array (
                'hash' => '17d99490-0997-11eb-a20e-a1294f6bcb06',
                'name' => 'Deposit',
                'description' => '{"string": "STRIPE", "match_type": "starts_with"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'credit',
                'transactions_categories_id' => '185003a0-eaf5-11ea-b5a2-350504b80f1b',
                'attention' => NULL,
                'approved' => 1,
                'created_at' => '2020-10-08 18:50:10',
                'updated_at' => '2020-10-08 21:23:15',
            ),
            2 => 
            array (
                'hash' => '25b7fc20-09af-11eb-9ea5-bbd532da95af',
                'name' => 'CC Payment',
                'description' => '{"string": "TD VISA", "match_type": "starts_with"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'debit',
                'transactions_categories_id' => '503294b0-ebe8-11ea-b0ef-13014a5132e8',
                'attention' => NULL,
                'approved' => NULL,
                'created_at' => '2020-10-08 21:42:22',
                'updated_at' => '2020-10-08 21:42:22',
            ),
            3 => 
            array (
                'hash' => '2c338420-0998-11eb-a9fe-5b3fbed8bc07',
                'name' => 'Bank Charges - Ignore',
                'description' => '{"string": "BODP FEE REBATE", "match_type": "contains"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'credit',
                'transactions_categories_id' => '843a1570-e3fc-11ea-bae3-e529d4dccd05',
                'attention' => NULL,
                'approved' => 1,
                'created_at' => '2020-10-08 18:57:54',
                'updated_at' => '2020-10-08 18:57:54',
            ),
            4 => 
            array (
                'hash' => '399c9b30-09ad-11eb-b568-5b5c0e5159ae',
                'name' => 'CC Payment',
                'description' => '{"string": "C", "match_type": "ends_with"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'debit',
                'transactions_categories_id' => '503294b0-ebe8-11ea-b0ef-13014a5132e8',
                'attention' => NULL,
                'approved' => 1,
                'created_at' => '2020-10-08 21:28:36',
                'updated_at' => '2020-10-08 21:32:05',
            ),
            5 => 
            array (
                'hash' => '96c99560-09aa-11eb-b0e0-4591663c9aa2',
                'name' => 'Monthly Banking Fee',
                'description' => '{"string": "MONTHLY PLAN FEE", "match_type": "starts_with"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'debit',
                'transactions_categories_id' => '843a1570-e3fc-11ea-bae3-e529d4dccd05',
                'attention' => NULL,
                'approved' => 1,
                'created_at' => '2020-10-08 21:09:44',
                'updated_at' => '2020-10-08 21:41:35',
            ),
            6 => 
            array (
                'hash' => 'c55ae450-09af-11eb-9590-770886fb4df3',
                'name' => 'MOBILE DEPOSIT',
                'description' => '{"string": "MOBILE DEPOSIT", "match_type": "contains"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'credit',
                'transactions_categories_id' => '185003a0-eaf5-11ea-b5a2-350504b80f1b',
                'attention' => NULL,
                'approved' => 1,
                'created_at' => '2020-10-08 21:46:49',
                'updated_at' => '2020-10-08 21:46:49',
            ),
            7 => 
            array (
                'hash' => 'daa10460-09af-11eb-a9ae-2dd36c043b92',
                'name' => 'Widthdraw',
                'description' => '{"string": "TD ATM W/D", "match_type": "starts_with"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'debit',
                'transactions_categories_id' => '75778900-0999-11eb-958d-45ba70374f1f',
                'attention' => NULL,
                'approved' => NULL,
                'created_at' => '2020-10-08 21:47:25',
                'updated_at' => '2020-10-08 21:47:25',
            ),
            8 => 
            array (
                'hash' => 'e010c340-09ac-11eb-a8a3-f38022c93bee',
                'name' => 'HST Payment',
                'description' => '{"string": "GST-P", "match_type": "starts_with"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'debit',
                'transactions_categories_id' => 'ab92cfd0-0999-11eb-9ee4-a103d7817f49',
                'attention' => NULL,
                'approved' => 1,
                'created_at' => '2020-10-08 21:26:06',
                'updated_at' => '2020-10-08 21:26:06',
            ),
            9 => 
            array (
                'hash' => 'e66015d0-0997-11eb-9241-ed6d1d9c92d0',
                'name' => 'Bank Charges - Ignore',
                'description' => '{"string": "BUSINESS ODP FEE", "match_type": "contains"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'debit',
                'transactions_categories_id' => '843a1570-e3fc-11ea-bae3-e529d4dccd05',
                'attention' => NULL,
                'approved' => 1,
                'created_at' => '2020-10-08 18:55:57',
                'updated_at' => '2020-10-08 18:57:03',
            ),
            10 => 
            array (
                'hash' => 'fdb5e730-0996-11eb-be9f-378f0120a8ed',
                'name' => 'Bank Charges - Taxes',
                'description' => '{"string": "TAX PYT FEE", "match_type": "starts_with"}',
                'amount' => '{"min": null}',
                'dates_range' => '{"end": null, "start": null}',
                'debit_credit' => 'debit',
                'transactions_categories_id' => '843a1570-e3fc-11ea-bae3-e529d4dccd05',
                'attention' => NULL,
                'approved' => 1,
                'created_at' => '2020-10-08 18:49:27',
                'updated_at' => '2020-10-08 18:49:27',
            ),
        ));
        
        
    }
}