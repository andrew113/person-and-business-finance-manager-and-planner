<?php

use Illuminate\Database\Seeder;

class VueRouterTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('vue_router')->delete();
        
        \DB::table('vue_router')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'users',
                'path' => '/admin/users',
                'componentPath' => '/pages/users/Users',
                'default' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'user-roles',
                'path' => '/admin/user-roles',
                'componentPath' => '/pages/users/UserRoles',
                'default' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'pathThrough',
                'path' => '/admin',
                'componentPath' => '#',
                'default' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'dashboard',
                'path' => '/dashboard',
                'componentPath' => '/pages/dashboard/Dashboard',
                'default' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'pathThrough',
                'path' => '/accounting',
                'componentPath' => '#',
                'default' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'transactions',
                'path' => '/accounting/transactions',
                'componentPath' => NULL,
                'default' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'accounts',
                'path' => '/accounting/accounts',
                'componentPath' => NULL,
                'default' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'transCategories',
                'path' => '/accounting/trans-categories',
                'componentPath' => NULL,
                'default' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'taxes',
                'path' => '/accounting/taxes',
                'componentPath' => NULL,
                'default' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'trans-import-rules',
                'path' => '/accounting/trans-import-rules',
                'componentPath' => NULL,
                'default' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'sales',
                'path' => '/sales',
                'componentPath' => NULL,
                'default' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'invoices',
                'path' => '/sales/invoices',
                'componentPath' => NULL,
                'default' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'subscriptions',
                'path' => '/sales/subcriptions',
                'componentPath' => NULL,
                'default' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'leads',
                'path' => '/sales/leads',
                'componentPath' => NULL,
                'default' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'contacts',
                'path' => '/sales/contacts',
                'componentPath' => NULL,
                'default' => NULL,
            ),
        ));
        
        
    }
}