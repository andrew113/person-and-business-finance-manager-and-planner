<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionImportRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_import_rules', function (Blueprint $table) {
            $table->uuid('hash')->unique();
            $table->primary('hash');
            $table->string('name');
            $table->json('description')->nullable();
            $table->json('amount')->nullable();
            $table->json('dates_range')->nullable();
            $table->string('debit_credit')->nullable();
            $table->string('transactions_categories_id')->nullable();
            $table->boolean('attention')->nullable();
            $table->boolean('approved')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_import_rules');
    }
}
