<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('hash')->unique();
            $table->primary('hash');
            $table->date('date');
            $table->string('account', 100)->nullable();
            $table->string('transactions_categories_id', 100)->nullable();
            $table->string('description', 100)->nullable();
            $table->double('debit')->nullable();
            $table->double('credit')->nullable();
            $table->mediumText('notes')->nullable();
            $table->boolean('approved')->nullable();
            $table->boolean('attention')->nullable();
            $table->string('use_type')->nullable();
            $table->string('import_rule_id')->nullable();
            $table->integer('import_batch_number')->nullable();
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
