<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJsonTableAssignableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('json_table_assignable', function (Blueprint $table) {
            $table->uuid('hash')->unique();
            $table->primary('hash');
            $table->json('table_data');
            $table->string('assignable_id', 50)->nullable();
            $table->string('assignable_type', 255)->nullable();
            $table->string('user_updated', 50)->nullable();
            $table->string('user_created', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('json_table_assignable');
    }
}
