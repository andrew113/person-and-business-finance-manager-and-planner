<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_categories', function (Blueprint $table) {
            $table->uuid('hash')->unique();
            $table->primary('hash');
            $table->string('name');
            $table->string('type', 20); //incoming / expense / other
            $table->string('expense_category',10)->nullable()->default('variable'); //variable /fixed
            $table->string('chart_color', 30)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions_categories');
    }
}
