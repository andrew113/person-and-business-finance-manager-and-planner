<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->uuid('hash')->unique();
            $table->primary('hash');
            $table->string('name', 100);
            $table->string('institution', 100)->nullable();
            $table->string('number', 100)->nullable();
            $table->string('type', 100); // bank account /credit card / credit line
            $table->string('use_type')->nullable(); //personal / business
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
