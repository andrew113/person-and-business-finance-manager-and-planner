import moment from 'moment'
import { CONF } from '@/config';

const mixin = {
	methods: {
        address(val) {
            var address = val[0]
            var string = '';

            if (address.unit) { string += address.unit + ' - '}
            if (address.address) { string += address.address }
            if (address.city) { 
                if ( string.length ) string += ', ';
                string += address.city
            }
            if (address.province) { 
                if ( string.length ) string += ', ';
                string += address.province
            }
            if (address.postal) { 
                if ( string.length ) string += ', ';
                string += address.postal
            }
            return string.length ? string : '-'
        },
        phoneObjNoType(val) {
            var string = '-';
            if (val) {
                string = this.phoneFormat(val[0].number)
                if (val[0].ext) {
                    string += 'x' + val[0].ext
                }
            }
            return string;
        },
        emailObjNoType(val) {
            if (val) {
                return val[0].email
            }
            return '-';
        },
        YesNo(value) {
            return value && value != 0 ? 'Yes' : 'No';
        },
        YesNoCheck(val) {
            return val && val != 0 ? '<i class="fas fa-check"></i>' : ''
        },
        dateFormat(value) {
            return value ? moment(value).format(CONF.DF_MOMENTS) : '-';
        },
		monthFormat(value) {
			return value ? moment(value).format('MMMM') : '-';
		},
        dateTimeFormat(value) {
            return value ? moment(value).format(CONF.MOMENT_DATETIME_HUMAN) : '-';
        },
        priceFormat(value) {
            return '$' + this.display_number(value);
        },
        priceFormatNoZero(value) {
            return value ? '$' + this.display_number(value) : '-';
        },
        percentFormat(value) {
            return '%' + this.display_number(value);
        },
        display_number: function(number, prefix, postfix, useGrouping) {
            if (typeof prefix == 'undefined') prefix = '';
            if (typeof postfix == 'undefined') postfix = '';
            if (typeof useGrouping == 'undefined') useGrouping = true;
            number = number * 1;
            return prefix + number.toLocaleString(undefined, {
                useGrouping: useGrouping,
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
            }, ) + postfix;
        },
        phoneFormat(value) {
            if (value && value.length == 10) {
                var cleaned = ('' + value).replace(/\D/g, '')
                var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
                return '(' + match[1] + ') ' + match[2] + '-' + match[3]
            } else if (value) {
                return value;
            }
            return '-';  
        },
        capitalize(value) {
            return value ? value.charAt(0).toUpperCase() + value.slice(1) : ''
        },
        toUpper(value) {
            return value.toUpperCase();
        },
        secondsToTime(value) {
            return moment.unix(value).utc().format('HH:mm:ss');
        }
    }
}
export default mixin
